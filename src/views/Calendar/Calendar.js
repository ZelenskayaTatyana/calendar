import Month from '@/views/Calendar/Month/Month.vue'
import dateHelper from '@/helpers/dateHelper'
import { mapState } from 'vuex'

export default {
  name: 'calendar',
  data: function () {
    return {
      currentDate: dateHelper.resetDateToMidnight(new Date()),
      isEventFormShown: false,
      animationName: undefined,
      monthParams: undefined,
      previousRoute: undefined,
      monthesNames: {
        0: 'January',
        1: 'February',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'August',
        8: 'September',
        9: 'October',
        10: 'November',
        11: 'December'
      },
      newEventData: {}
    }
  },
  created: function () {
    this.updateSomeInitialData()
  },
  watch: {
    '$route' (to, from) {
      // saving prev route for determining animation direction
      this.previousRoute = from

      this.updateSomeInitialData()
    }
  },
  methods: {
    updateSomeInitialData: function () {
      this.monthParams = undefined

      let { month, year } = {
        month: +this.$route.params.month,
        year: +this.$route.params.year
      }

      if (typeof month === 'number' && month > 0 && month < 13) {
        month = month - 1
      } else {
        month = this.currentDate.getMonth()
      }

      if (!(typeof year === 'number' && year > 1970 && year < 275761)) {
        year = this.currentDate.getFullYear()
      }

      // checking if route params was incorrect
      if ((month + 1) !== +this.$route.params.month || year !== +this.$route.params.year) {
        const rightRouteParams = {
          month: month + 1,
          year: year
        }

        // replacing route params with valid data
        this.$router.replace({ name: 'calendar', params: rightRouteParams })
      } else {
        // determining of animation direction
        if (this.previousRoute) {
          const firstDateOfOldMonth = new Date(this.previousRoute.params.year + '-' + this.previousRoute.params.month)
          const firstDateOfNewMonth = new Date(year + '-' + (month + 1))

          if ((firstDateOfNewMonth - firstDateOfOldMonth) >= 0) {
            this.animationName = 'sliding-forward'
          } else {
            this.animationName = 'sliding-backward'
          }
        }

        setTimeout(() => { // for animation working
          this.monthParams = { month, year }
        }, 0)
      }
    },
    slideToNextMonth: function () {
      let newRouteParams = {}

      newRouteParams.month = this.monthParams.month === 11 ? 1 : this.monthParams.month + 2
      newRouteParams.year = newRouteParams.month <= this.monthParams.month ? this.monthParams.year + 1 : this.monthParams.year

      this.$router.push({ name: 'calendar', params: newRouteParams })
    },
    slideToPrevMonth: function () {
      let newRouteParams = {}

      newRouteParams.month = this.monthParams.month === 0 ? 12 : this.monthParams.month
      newRouteParams.year = newRouteParams.month > this.monthParams.month ? this.monthParams.year - 1 : this.monthParams.year

      this.$router.push({ name: 'calendar', params: newRouteParams })
    },
    openEventForm: function () {
      this.newEventData = {
        name: '',
        types: []
      }

      if (this.selectedDateInfo) {
        const defaultDate = new Date(+this.selectedDateInfo.date + 86400000).toISOString().slice(0, 10)
        this.newEventData.start = defaultDate
        this.newEventData.end = defaultDate
      }

      this.isEventFormShown = true
    },
    checkAndSubmitEventForm: function (e) {
      e.preventDefault()

      this.newEventData.start = new Date(this.newEventData.start)
      this.newEventData.end = new Date(this.newEventData.end)

      this.newEventData.start = dateHelper.resetDateToMidnight(this.newEventData.start)
      this.newEventData.end = dateHelper.resetDateToMidnight(this.newEventData.end)

      if (this.newEventData.start <= this.newEventData.end) {
        this.createEvent(this.newEventData).then((response) => {
          this.$root.$emit('event:new', response)
          this.isEventFormShown = false

          if (this.selectedDateInfo &&
            dateHelper.checkIsDateInRange(this.selectedDateInfo.date, this.newEventData)) {
            setTimeout(() => {
              window.scrollTo({
                top: document.body.scrollHeight,
                behavior: 'smooth'
              })
            }, 250) // 250ms is duration of fade transition for events appearing
          }
        })
      } else {
        alert('Start date should not be later than end.')
      }
    },
    createEvent: function (newEvent) {
      return this.$store.dispatch('createEvent', newEvent)
    },
    deleteEvent: function (eventId) {
      let confirmation = confirm('Are you sure you want to remove this event?')
      if (confirmation) {
        return this.$store.dispatch('deleteEvent', eventId)
      } else {
        return Promise.reject(Error('canceled_deletion'))
      }
    }
  },
  computed: {
    pageTitle: function () {
      return this.monthesNames[this.monthParams.month] + ', ' + this.monthParams.year
    },
    ...mapState([
      'events',
      'eventsTypes',
      'unremovableEventsTypes',
      'selectedDateInfo'
    ])
  },
  components: {
    Month
  }
}
