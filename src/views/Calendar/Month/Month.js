import dateHelper from '@/helpers/dateHelper'
import { mapState } from 'vuex'

export default {
  name: 'Month',
  props: {
    monthParams: Object,
    events: Array,
    unremovableEventsTypes: Array,
    deleteEvent: Function
  },
  data: function () {
    return {
      shownDates: [],
      currenDate: undefined
    }
  },
  created: function () {
    this.currenDate = dateHelper.resetDateToMidnight(new Date())

    this.$store.dispatch('updateSelectedDateInfo', undefined)

    this.$root.$on('event:new', (event) => {
      this.tryToAppendEvent(event)
    })

    this.getShownDates()
  },
  methods: {
    getShownDates: function () {
      this.shownDates = []
      let { month, year } = this.monthParams
      let weekDayOfTheFirstMonthDate
      let monthSize
      let i
      let countOfDays

      monthSize = 32 - new Date(year, month, 32).getDate()

      weekDayOfTheFirstMonthDate = new Date(year, month, 1).getDay()
      weekDayOfTheFirstMonthDate = weekDayOfTheFirstMonthDate === 0 ? 7 : weekDayOfTheFirstMonthDate

      countOfDays = (weekDayOfTheFirstMonthDate + monthSize) > 36 ? 42 : 35

      if (weekDayOfTheFirstMonthDate !== 1) {
        for (i = weekDayOfTheFirstMonthDate - 2; i >= 0; i--) {
          let calculatedDate = new Date(year, month, 1)
          calculatedDate.setDate(i * -1)
          this.shownDates.push({ date: calculatedDate, events: [] })
        }
      }

      for (i = 1; i <= monthSize; i++) {
        let calculatedDate = new Date(year, month, 1)
        calculatedDate.setDate(i)
        this.shownDates.push({ date: calculatedDate, events: [] })
      }

      if (this.shownDates.length < countOfDays) {
        let countOfRestEmptyDays = countOfDays - this.shownDates.length

        for (i = 1; i <= countOfRestEmptyDays; i++) {
          let calculatedDate = new Date(year, month, 1)
          calculatedDate.setDate(monthSize + i)
          this.shownDates.push({ date: calculatedDate, events: [] })
        }
      }

      this.events.forEach(eventItem => {
        this.tryToAppendEvent(eventItem)
      })
    },
    tryToAppendEvent: function (event) {
      this.shownDates.forEach((dateItem, i) => {
        const eventStart = event.start
        const eventEnd = event.end
        const dateStart = dateItem.date

        if (dateStart >= eventStart && dateStart <= eventEnd) {
          this.shownDates[i].events.push(event)
        }
      })
    },
    tryToDetachEvent: function (eventId) {
      this.shownDates.forEach((dateItem, i) => {
        let eventIndex = dateItem.events.findIndex((event) => {
          return event.id === eventId
        })

        if (eventIndex !== -1) {
          this.shownDates[i].events.splice(eventIndex, 1)
          return 1
        }
      })
    },
    getSpecialClasses: function (dateItem) {
      return {
        'month__day--event': dateItem.events.length,
        'month__day--day-off': this.checkIsDateDayOff(dateItem.events),
        'month__day--beginning-event': this.checkIsDateBeginningOfEvent(dateItem),
        'month__day--continuation-event': this.checkIsDateContinuationOfEvent(dateItem),
        'month__day--from-other-month': dateItem.date.getMonth() !== this.monthParams.month,
        'month__weekend': dateItem.date.getDay() === 6 || dateItem.date.getDay() === 0,
        'month__day--current-day': +dateItem.date === +this.currenDate,
        'month__day--selected': this.selectedDateInfo && +dateItem.date === +this.selectedDateInfo.date
      }
    },
    checkIsDateDayOff: function (eventsSet) {
      return eventsSet.some((event) => {
        return event.types.some((type) => {
          return type[1] === 'day_off'
        })
      })
    },
    checkIsDateBeginningOfEvent: function (dateItem) {
      const nextDate = +(new Date(+dateItem.date + 86400000))

      return dateItem.events.some((dateEvent) => {
        const dateEventId = dateEvent.id
        return this.events.some((event) => {
          // this is for showing connections even with dates that are not displayed
          return event.id === dateEventId && nextDate >= +event.start && nextDate <= +event.end
        })
      })
    },
    checkIsDateContinuationOfEvent: function (dateItem) {
      const prevDate = +(new Date(+dateItem.date - 86400000))

      return dateItem.events.some((dateEvent) => {
        const dateEventId = dateEvent.id
        return this.events.some((event) => {
          // this is for showing connections even with dates that are not displayed
          return event.id === dateEventId && prevDate >= +event.start && prevDate <= +event.end
        })
      })
    },
    checkIsEventUnremovable: function (eventTypes) {
      return this.unremovableEventsTypes.some((unremovableType) => {
        return eventTypes.some((type) => {
          return type[0] === unremovableType
        })
      })
    },
    removeEvent: function (eventId) {
      this.deleteEvent(eventId).then(() => {
        this.tryToDetachEvent(eventId)
      }).catch(err => console.log(err))
    },
    selectDate: function (dateItem) {
      this.$store.dispatch('updateSelectedDateInfo', dateItem)

      if (dateItem.events.length) {
        setTimeout(() => {
          window.scrollTo({
            top: document.body.scrollHeight,
            behavior: 'smooth'
          })
        }, 250) // 250ms is duration of fade transition for events appearing
      }
    }
  },
  computed: {
    ...mapState([
      'selectedDateInfo'
    ])
  }
}
