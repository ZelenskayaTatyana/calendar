import Vue from 'vue'
import Router from 'vue-router'
import Calendar from './views/Calendar/Calendar.vue'
import store from './store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/:month?/:year?',
      name: 'calendar',
      component: Calendar,
      beforeEnter: function (to, from, next) {
        store.dispatch('getData').then(() => {
          next()
        }).catch(() => {
          alert('Something went wrong')
        })
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
