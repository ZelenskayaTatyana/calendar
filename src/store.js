import Vue from 'vue'
import Vuex from 'vuex'

import dateHelper from './helpers/dateHelper'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    events: undefined,
    eventsTypes: undefined,
    unremovableEventsTypes: undefined,
    selectedDateInfo: undefined
  },
  mutations: {
    updateData: function (state, data) {
      let events = []

      data.events.forEach(event => {
        let formattedEvent = {
          id: event.id,
          start: new Date(event.starts_at),
          end: new Date(event.ends_at),
          name: event.memo,
          types: []
        }

        formattedEvent.start = dateHelper.resetDateToMidnight(formattedEvent.start)
        formattedEvent.end = dateHelper.resetDateToMidnight(formattedEvent.end)

        data.meta.types.forEach(type => {
          if (event.type & type[0]) {
            formattedEvent.types.push(type)
          }
        })

        events.push(formattedEvent)
      })

      state.events = events
      state.eventsTypes = data.meta.types
      state.unremovableEventsTypes = data.meta.unremovable
    },
    setEvents: function (state, events) {
      state.events = events
    },
    selectedDateInfo: function (state, dateInfo) {
      state.selectedDateInfo = dateInfo
    }
  },
  actions: {
    getData: function (context) {
      return fetch('./static/Calendar_data.json', { method: 'GET' })
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          context.commit('updateData', data)
        })
        .catch((err) => {
          throw err
        })
    },
    deleteEvent: function (context, eventId) {
      return new Promise((resolve, reject) => {
        const deletedEventIndex = this.state.events.findIndex((event) => {
          return event.id === eventId
        })

        if (deletedEventIndex !== -1) {
          this.state.events.splice(deletedEventIndex, 1)
          context.commit('setEvents', this.state.events)
          resolve()
        } else {
          reject(new Error('Event not found'))
        }
      })
    },
    createEvent: function (context, newEvent) {
      return new Promise((resolve) => {
        this.state.events.sort((prev, next) => {
          return prev.id - next.id
        })
        const maxId = this.state.events[this.state.events.length - 1].id
        newEvent.id = maxId + 1
        this.state.events.push(newEvent)
        context.commit('setEvents', this.state.events)
        resolve(newEvent)
      })
    },
    updateSelectedDateInfo: function (context, newDateInfo) {
      context.commit('selectedDateInfo', newDateInfo)
    }
  }
})
