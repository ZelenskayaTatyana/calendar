export default {
  resetDateToMidnight: function (date) {
    date.setHours(0)
    date.setMinutes(0)
    date.setSeconds(0)
    date.setMilliseconds(0)
    return date
  },
  checkIsDateInRange: function (checkingDate, range) {
    return checkingDate <= range.end && checkingDate >= range.start
  }
}
