export default {
  name: 'Modal',
  props: {
    isShown: Boolean
  }
}
